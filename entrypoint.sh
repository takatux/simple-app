#!/bin/sh

npm run migrate
npm run seed

# This will exec the CMD from your Dockerfile, i.e. "npm start"
exec "$@"
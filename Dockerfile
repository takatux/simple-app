FROM node:alpine3.14

# Please continue here
#USER node
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY . .
RUN npm install
RUN chmod +x ./entrypoint.sh
ENTRYPOINT [ "./entrypoint.sh" ]
CMD [ "npm", "start" ]
